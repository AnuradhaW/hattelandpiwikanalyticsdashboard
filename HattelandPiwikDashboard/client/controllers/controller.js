﻿var myApp = angular.module('myApp', []);

myApp.controller('AppCtrl', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {
        
        var PiwikClient = require('piwik-client');  // a core module
        //var PythonShell = require('python-shell');
        console.log(PiwikClient);
        //var myClient = new PiwikClient('http://localhost/piwik/index.php', 'f9ba66405ac04df5e4be32789f1bb099');
        var myClient = new PiwikClient('http://adaptiveui.tk///piwik/index.php', 'bf8535dfb2511c611da95bd9a8e63e57');
        console.log(myClient);

        $scope.title = "Hatteland Piwik Analytics Dashboard";

        $scope.currentWebsiteId = '';
        
        $scope.userId;

        //$scope.firstLoad = true;
        
        $scope.list = [];
        $scope.visitorLogs = [];
        
        $scope.period = "month";
        $scope.blockPeriod = "month";
        $scope.blockGraph = "visits";

        $scope.eventActionPeriod = "month";
        $scope.eventCategory = '';

        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        var getEventCategoryFromScope = false;

        $scope.date = year + '-' + month + '-' + day;
        $scope.dateFrom = year + '-' + month + '-' + day;
        $scope.dateTo = year + '-' + month + '-' + day;

        $scope.eventActionDate = year + '-' + month + '-' + day;
        $scope.eventActionDateTo = year + '-' + month + '-' + day;
        $scope.eventActionDateFrom = year + '-' + month + '-' + day;
        
        $scope.range = 'range';

        $scope.blockRange = 'last';
        $scope.prevBlockAmount = '';
        $scope.lastBlockAmount = '10';

        $scope.eventActionRange = 'range';

        $scope.resultsAvailable = 'no';

        $scope.globalEvents = [];

        $scope.selectedUser = false;
        $scope.generateProfile = false;
        
        myClient.api({
            method: 'SitesManager.getAllSites'
        }, function (err, responseObject) {
            if (!err) {
                $scope.websites = responseObject;
                console.log(responseObject);
                console.log("response: " + JSON.stringify(responseObject));
                for (var k in responseObject) {
                    $scope.list[k] = responseObject[k];
                }
                $scope.$apply();
            } else {
                console.log(err);
            }
        });
        
        $scope.createWebsite = function () {
            console.log(PiwikClient);
        }

        $scope.getData = function(queryUserId) {
            $scope.selectedUser = true;
            $scope.generateProfile = false;
            $scope.resultsAvailable = 'no';

            console.log($scope.globalEvents);
            $scope.dataToBeSent = '';
            generateDataToBeSentToAWS(queryUserId);

            drawUsageChartToUser(queryUserId);

            $http.post('http://adaptiveui.tk:1338/dowork/109',{"msg":"hi"}).success(function(data, status, headers, config){
                //alert('dataaaaaa: '+data);
            }).
            error(function(data, status, headers, config) {
              console.log('error');
            });
            //alert(queryUserId);
        }

        $scope.generateDataFile = function() {

            if ($scope.startLoading != 'yes') {
                    //alert("nooooooo");
                    //console.log("nooooooo");
                    $scope.generate = false;
            } else {

                $scope.generate = true;

                document.getElementById("generate_file").disabled = true;

                $scope.visiblePane = false;
                $scope.visiblePaneProgress = 'pending';

                $scope.batchData = "";

                var csvData = "Id";
                

                for(var k in $scope.globalEvents) {
                    var e = $scope.globalEvents[k];
                    csvData += ","+e+"_visits,"+e+"_events,"+e+"_events_with_value,"+e+"_sum_event_value,"+e+"_min_event_value,"+e+"_max_event_value,"+e+"_sum_daily_nb_uniq_visitors,"+e+"_avg_event_value";
                }

                csvData += "\n";

                $scope.rowUserIdIndex = 0;

                recursuveCall(0,csvData);
            }
        }

        $scope.simulateEndpoint = function() {
            $http.get('http://adaptiveui.tk:1338/getProfile/1/Tharindu').success(function(data, status, headers, config){
                alert('result: '+data);
            }).
            error(function(data, status, headers, config) {
                console.log('error');
            });
        }

        function recursuveCall(rowUserIdIndex,csvData) {

                    var segmentVar = 'userId==' + $scope.userIds[rowUserIdIndex];

                    var arr = $scope.website.ts_created.split(" ");
                    var createdDate = arr[0];

                    //if(rowUserIdIndex == k) {
                        myClient.api({
                            method: 'Events.getCategory',
                            idSite: $scope.website.idsite,
                            period: 'range',
                            date: ''+createdDate+',today',
                            segment: segmentVar
                        }, function (err, responseObject) {

                            console.log("second: "+segmentVar);

                            //console.log($scope.userIds[rowUserIdIndex]);
                            console.log(responseObject);

                            csvData += ""+$scope.userIds[rowUserIdIndex];

                            var len = $scope.userIds.length;

                            // write data to csv batch file to be sent to AWS
                            for(var k in $scope.globalEvents) {
                                var found = false;
                                var e = $scope.globalEvents[k];
                                for(var m in responseObject) {
                                    if($scope.globalEvents[k] == responseObject[m].label) {
                                        //console.log(responseObject[m].label);
                                        csvData += ","+responseObject[m].nb_visits+","+responseObject[m].nb_events+","+responseObject[m].nb_events_with_value+","+responseObject[m].sum_event_value+","+responseObject[m].min_event_value+","+responseObject[m].max_event_value+","+responseObject[m].sum_daily_nb_uniq_visitors+","+responseObject[m].avg_event_value;
                                        found = true;
                                        break;
                                    }
                                }
                                if(!found) {
                                    csvData += ",0,0,0,0,0,0,0,0";
                                }
                            }

                            csvData += "\n";

                            rowUserIdIndex++;

                            if(rowUserIdIndex == len) {
                                //console.log(csvData);
                                $scope.batchData = csvData;
                                $scope.visiblePane = true;
                                $scope.visiblePaneProgress = 'yes';
                                document.getElementById("generate_file").disabled = false;
                                $http.post('http://adaptiveui.tk:1338/writeBatchData',{"batch":csvData}).success(function(data, status, headers, config){
                                    //alert('dataaaaaa: '+data);
                                }).
                                error(function(data, status, headers, config) {
                                  console.log('error');
                                });
                            } else {
                                recursuveCall(rowUserIdIndex,csvData);
                            }

                        });
                    //} else {
                    //    recursuveCall(k, k, csvData);
                    //} 
                
                        
        }

        function generateDataToBeSentToAWS(queryUserId) {

            var segmentVar = 'userId==' + queryUserId;

            var arr = $scope.website.ts_created.split(" ");
            var createdDate = arr[0];

            myClient.api({
                method: 'Events.getCategory',
                idSite: $scope.website.idsite,
                period: 'range',
                date: ''+createdDate+',today',
                segment: segmentVar
            }, function (err, responseObject) {
                if (!err) {
                    console.log(JSON.stringify(responseObject));

                    var jsonData = '{"Id":"'+queryUserId+'",';

                    var len = $scope.globalEvents.length - 1;

                    // write json data to file to be sent to AWS
                    for(var k in $scope.globalEvents) {
                        var found = false;
                        var e = $scope.globalEvents[k];
                        for(var m in responseObject) {
                            if($scope.globalEvents[k] == responseObject[m].label) {
                                console.log(responseObject[m].label);
                                jsonData += '"'+e+'_visits":"'+responseObject[m].nb_visits+'","'+e+'_events":"'+responseObject[m].nb_events+'","'+e+'_events_with_value":"'+responseObject[m].nb_events_with_value+'","'+e+'_sum_event_value":"'+responseObject[m].sum_event_value+'","'+e+'_min_event_value":"'+responseObject[m].min_event_value+'","'+e+'_max_event_value":"'+responseObject[m].max_event_value+'","'+e+'_sum_daily_nb_uniq_visitors":"'+responseObject[m].sum_daily_nb_uniq_visitors+'","'+e+'_avg_event_value":"'+responseObject[m].avg_event_value+'"';
                                if(k!=len) {
                                    jsonData += ',';    
                                }
                                found = true;
                                break;
                            }
                        }
                        if(!found) {
                            jsonData += '"'+e+'_visits":"0","'+e+'_events":"0","'+e+'_events_with_value":"0","'+e+'_sum_event_value":"0","'+e+'_min_event_value":"0","'+e+'_max_event_value":"0","'+e+'_sum_daily_nb_uniq_visitors":"0","'+e+'_avg_event_value":"0"';
                            if(k!=len) {
                                jsonData += ',';  
                            }
                        }
                    }

                    jsonData += '}';

                    var jsonObj = JSON.parse(jsonData);

                    console.log(jsonObj);
                    $scope.dataToBeSent = JSON.stringify(jsonObj, null, 82);
                    $scope.generateProfile = true;

                    $http.post('http://adaptiveui.tk:1338/writeData',jsonData).success(function(data, status, headers, config){
                        //alert('dataaaaaa: '+data);
                    }).
                    error(function(data, status, headers, config) {
                      console.log('error');
                    });


                } else {
                    console.log(err);
                }
            });
        }

        $scope.posts = '';

        $scope.getDataToScreen = function() {

            $scope.resultsAvailable = 'no';
            
            $http.get('../python/result.txt').success(function(data, status, headers, config) {
              $scope.posts = data;
            }).
            error(function(data, status, headers, config) {
              console.log('error');
            });
            //alert($scope.posts);
            var resultStr = $scope.posts.split("You are currently ");

            $scope.result = resultStr[0];
            $scope.profileName = "You currently belong to profile: "+ resultStr[1]; 
            $scope.profileSrc = 'data/'+$scope.currentWebsiteId+'/profile/'+resultStr[1]+'.png';
            $scope.customizedLayoutSrc = 'data/'+$scope.currentWebsiteId+'/layout/'+resultStr[1]+'.png';

            if($scope.result != '') {
                $scope.resultsAvailable = 'yes';
            } else {
                $scope.resultsAvailable = 'pending';
            }

            
        }

        $scope.changeWebsiteInfo = function (websiteId) {
            //alert('triggered' + websiteId);
            $scope.currentWebsiteId = websiteId;
            $scope.globalEvents = [];
            $scope.dataToBeSent = '';
            $scope.selectedUser = false;
            $scope.batchData = "";
            $scope.visiblePane = false;
            $scope.visiblePaneProgress = 'no';
            $scope.visitorLogs = [];
            $scope.startLoading = 'no';
            $scope.generate = true;

            var arr = $scope.website.ts_created.split(" ");
            var createdDate = arr[0];

            console.log("start");

            $scope.startLoading = 'pending';

            myClient.api({
                method: 'Live.getLastVisitsDetails',
                idSite: websiteId,
                period: 'range',
                date: ''+createdDate+',today'
            }, function (err, responseObject) {
                //console.log(JSON.stringify(responseObject));
                $scope.visitorLogs = responseObject;
                $scope.userIds = [];

                //var i = 0;
                for(var k in $scope.visitorLogs) {
                    var len = $scope.visitorLogs.length - 1;
                    //alert(len + " "+k);
                    //console.log(k);
                    var id = $scope.visitorLogs[k].userId;
                    if($scope.userIds.indexOf(id) == -1) {
                        $scope.userIds.push(id);
                    }
                }
                $scope.startLoading = 'yes';
                $scope.$apply();
                console.log("doneeeeeeee");
                $scope.generate = true;
            });


            $http.get('../data/'+$scope.currentWebsiteId+'/json/layout.json').success(function(data, status, headers, config) {
              $scope.layout = data;
              var rows = $scope.layout.rows;
                for(var k in rows) {
                    var cols = rows[k].cols;
                    for(var m in cols) {
                        var eventCategory = cols[m].feature.eventCategory;
                        $scope.globalEvents.push(eventCategory);
                    }
                }
            }).
            error(function(data, status, headers, config) {
              console.log('error');
            });

            // if(websiteId == 1) {
            //     $scope.userIds = [
            //         "Tharindu",
            //         "abc-123-rst",
            //         "abc-123-uvw",
            //         "abc-123-xyz",
            //         "abc-456-rst",
            //         "abc-456-uvw",
            //         "abc-456-xyz",
            //         "abc-789-rst",
            //         "abc-789-uvw",
            //         "abc-789-xyz",

            //         "def-123-rst",
            //         "def-123-uvw",
            //         "def-123-xyz",
            //         "def-456-rst",
            //         "def-456-uvw",
            //         "def-456-xyz",
            //         "def-789-rst",
            //         "def-789-uvw",
            //         "def-789-xyz",

            //         "ghi-123-rst",
            //         "ghi-123-uvw",
            //         "ghi-123-xyz",
            //         "ghi-456-rst",
            //         "ghi-456-uvw",
            //         "ghi-456-xyz",
            //         "ghi-789-rst",
            //         "ghi-789-uvw",
            //         "ghi-789-xyz",
            //     ];
            // }
            
            $scope.defaultLayoutSrc = 'data/'+websiteId+'/layout/default.png';
        }

        $scope.changeUserId = function (userId) {
            $scope.userId = userId;
            plotEvents('events');
            $scope.LoadAndPlotEventActions();
            plotTimelineEvents();
        }
        
        $scope.changeComboBoxPeriod = function (type, period) {
            if(type == 'events') {
                $scope.period = period;
            } else if (type == 'eventActions'){
                $scope.eventActionPeriod = period;
                document.getElementById("changeCategoryBtn").disabled = true;
            } else if(type == 'eventNames') {

            }
        }
        
        $scope.changeCalendarDate = function (type, date) {
            if(type == 'events') {
                $scope.date = date;
            } else if (type == 'eventActions'){
                $scope.eventActionDate = date;
                document.getElementById("changeCategoryBtn").disabled = true;
            } else if(type == 'eventNames') {

            }
        }
        
        $scope.changeCalendarDateFrom = function (type, dateFrom) {
            if(type == 'events') {
                $scope.dateFrom = dateFrom;
            } else if (type == 'eventActions'){
                $scope.eventActionDateFrom = dateFrom;
                document.getElementById("changeCategoryBtn").disabled = true;
            } else if(type == 'eventNames') {

            }
        }
        
        $scope.changeCalendarDateTo = function (type, dateTo) {
            if(type == 'events') {
                $scope.dateTo = dateTo;
            } else if (type == 'eventActions'){
                $scope.eventActionDateTo = dateTo;
                document.getElementById("changeCategoryBtn").disabled = true;
            } else if(type == 'eventNames') {

            }
        }

        $scope.changePeriod = function (type) {
            plotEvents(type);
        }
        
        $scope.changeTimelinePeriod = function () {
            plotTimelineEvents();
        }
        
        $scope.blockGraphChange = function (blockGraph) {
            $scope.blockGraph = blockGraph;
        }
        
        $scope.rangeClick = function (type, range) {
            if(type == 'events') {
                $scope.range = range;
                document.getElementById("dateFrom").disabled = false;
                document.getElementById("dateTo").disabled = false;
                document.getElementById("prevText").disabled = true;
                document.getElementById("lastText").disabled = true;
            } else if (type == 'eventActions'){
                $scope.eventActionRange = range;
                document.getElementById("eventActionDateFrom").disabled = false;
                document.getElementById("eventActionDateTo").disabled = false;
                document.getElementById("eventActionPrevText").disabled = true;
                document.getElementById("eventActionLastText").disabled = true;

                document.getElementById("changeCategoryBtn").disabled = true;

            } else if(type == 'eventNames') {

            }
            
        }
        
        $scope.prevClick = function (type, range) {
            if(type == 'events') {
                $scope.range = range;
                document.getElementById("dateFrom").disabled = true;
                document.getElementById("dateTo").disabled = true;
                document.getElementById("prevText").disabled = false;
                document.getElementById("lastText").disabled = true;
            } else if (type == 'eventActions'){
                $scope.eventActionRange = range;
                document.getElementById("eventActionDateFrom").disabled = true;
                document.getElementById("eventActionDateTo").disabled = true;
                document.getElementById("eventActionPrevText").disabled = false;
                document.getElementById("eventActionLastText").disabled = true;

                document.getElementById("changeCategoryBtn").disabled = true;

            } else if(type == 'eventNames') {

            }
            
        }
        
        $scope.lastClick = function (type, range) {
            if(type == 'events') {
                $scope.range = range;
                document.getElementById("dateFrom").disabled = true;
                document.getElementById("dateTo").disabled = true;
                document.getElementById("prevText").disabled = true;
                document.getElementById("lastText").disabled = false;
            } else if (type == 'eventActions'){
                $scope.eventActionRange = range;
                document.getElementById("eventActionDateFrom").disabled = true;
                document.getElementById("eventActionDateTo").disabled = true;
                document.getElementById("eventActionPrevText").disabled = true;
                document.getElementById("eventActionLastText").disabled = false;

                document.getElementById("changeCategoryBtn").disabled = true;

            } else if(type == 'eventNames') {

            }
            
        }
        
        $scope.blockPeriodChange = function (blockPeriod) {
            $scope.blockPeriod = blockPeriod;
        }
        
        $scope.blockPrevClick = function (range) {
            $scope.blockRange = range;
            document.getElementById("prevBlockText").disabled = false;
            document.getElementById("lastBlockText").disabled = true;
        }
        
        $scope.blockLastClick = function (range) {
            $scope.blockRange = range;
            document.getElementById("prevBlockText").disabled = true;
            document.getElementById("lastBlockText").disabled = false;
        }

        function plotEvents(type) {

            var methodVar = '';
            var dateVar = '';
            var segmentVar = '';
            var period = '';

            if(type == 'events') {
                methodVar = 'Events.getCategory';
                segmentVar = 'userId==' + $scope.userId;
                period = $scope.period;

                if ($scope.period == 'range') {
                    if ($scope.range == 'range') {
                        dateVar = $scope.dateFrom + "," + $scope.dateTo;
                    } else if ($scope.range == 'previous') {
                        dateVar = "previous"+document.getElementById("prevText").value;
                    } else if ($scope.range == 'last') {
                        dateVar = "last" + document.getElementById("lastText").value;
                    } 
                } else {
                    dateVar = $scope.date;
                }

            } else if (type == 'eventActions') {

                if(!getEventCategoryFromScope) {
                    if(document.getElementById("eventsCombo") != null) {
                        $scope.eventCategory = document.getElementById("eventsCombo").value;
                    }
                } else {
                    getEventCategoryFromScope = false;
                }

                //alert($scope.eventCategory);

                methodVar = 'Events.getAction';
                segmentVar = 'userId==' + $scope.userId + ';eventCategory=='+$scope.eventCategory;
                period = $scope.eventActionPeriod;

                if ($scope.eventActionPeriod == 'range') {
                    if ($scope.eventActionRange == 'range') {
                        dateVar = $scope.eventActionDateFrom + "," + $scope.eventActionDateTo;
                    } else if ($scope.eventActionRange == 'previous') {
                        dateVar = "previous"+ document.getElementById("eventActionPrevText").value;
                    } else if ($scope.eventActionRange == 'last') {
                        dateVar = "last" + document.getElementById("eventActionLastText").value;
                    } 
                } else {
                    dateVar = $scope.eventActionDate;
                }


            } else if (type == 'eventNames') {
                methodVar = 'Events.getName';
                segmentVar = 'userId==' + $scope.userId; 
            }

            myClient.api({
                method: methodVar,
                idSite: $scope.website.idsite,
                period: period,
                date: dateVar,
                segment: segmentVar
            }, function (err, responseObject) {
                //alert(JSON.stringify(responseObject));
                console.log("firsttt: "+segmentVar+" "+type);
                plotGraph(err, responseObject, type);
            });
        }

        function drawUsageChartToUser(userId) {
            var segmentVar = 'userId==' + userId;

            var arr = $scope.website.ts_created.split(" ");
            var createdDate = arr[0];

            var visits = [];
            var events = [];
            var valueEvents = [];
            var sum = [];
            var avg = [];

            myClient.api({
                method: 'Events.getCategory',
                idSite: $scope.website.idsite,
                period: 'range',
                date: ''+createdDate+',today',
                segment: segmentVar
            }, function (err, responseObject) {

                var treemapSeriesChildren = [];

                var enableTreeChart = false;
                
                for(var k in $scope.globalEvents) {
                    var found = false;
                    var e = $scope.globalEvents[k];
                    for(var m in responseObject) {
                        if($scope.globalEvents[k] == responseObject[m].label) {
                            //console.log(responseObject[m].label);
                            visits[k] = responseObject[m].nb_visits;
                            events[k] = responseObject[m].nb_events;
                            valueEvents[k] = responseObject[m].nb_events_with_value;
                            sum[k] = responseObject[m].sum_event_value;
                            avg[k] = responseObject[m].avg_event_value;

                            treemapSeriesChildren.push({"text":$scope.globalEvents[k],"value":sum[k]});

                            if(sum[k] != 0) {
                                enableTreeChart = true;
                            }

                            found = true;
                            break;
                        }
                    }
                    if(!found) {
                        visits[k] = 0;
                        events[k] = 0;
                        valueEvents[k] = 0;
                        sum[k] = 0;
                        avg[k] = 0;
                        treemapSeriesChildren.push({"text":$scope.globalEvents[k],"value":0});
                    }
                }

                // drawing the graph

                var chartData = {
                    "type": "bar", 
                    "legend": {},
                    "plotarea": {
                        "adjust-layout": true
                    },
                    "scale-x": {
                        "label": { 
                            "text": 'Event Category',
                        },
                        "labels": $scope.globalEvents
                    },
                    "series": [ 
                        { "values": visits, "text": "Visits", "background-color": "#3c763d", "alpha": 0.9, "hover-state": { "background-color": "#3c763d" } },
                        { "values": events, "text": "Events", "background-color": "#f2dede", "alpha": 0.9, "hover-state": { "background-color": "#f2dede" } },
                        { "values": valueEvents, "text": "Events with value", "background-color": "#5bc0de", "alpha": 0.9, "hover-state": { "background-color": "#5bc0de" } },
                        { "values": sum, "text": "Sum event value", "background-color": "#d9534f", "alpha": 0.9, "hover-state": { "background-color": "#d9534f" } },
                        { "values": avg, "text": "Avg event value", "background-color": "#f0ad4e", "alpha": 0.9, "hover-state": { "background-color": "#f0ad4e" } }
                    ]
                };

                zingchart.render({
                    id: 'queryUserChartDiv',
                    data: chartData
                });

                treemapData = {
                    "type":"treemap",
                    "series":[
                        {
                            "text":"Events map",
                            "children":treemapSeriesChildren
                        }
                    ]
                };

                treemapNullData = {
                    "type":"treemap",
                    "series":[
                        {
                            "text":"Events map",
                            "children":[]
                        }
                    ]
                };

                if(enableTreeChart) {
                    zingchart.render({
                        id: 'queryUserTreeChartDiv',
                        data: treemapData
                    });
                } else {
                    zingchart.render({
                        id: 'queryUserTreeChartDiv',
                        data: treemapNullData
                    });
                }

                

            });
        }

        /* event categories loading into combo box */
        $scope.LoadAndPlotEventActions = function() {
            var methodVar = 'Events.getCategory';
            var dateVar = '';
            var segmentVar = '';
            var period = '';

            segmentVar = 'userId==' + $scope.userId;
            period = $scope.eventActionPeriod;

            if ($scope.eventActionPeriod == 'range') {
                if ($scope.eventActionRange == 'range') {
                    dateVar = $scope.eventActionDateFrom + "," + $scope.eventActionDateTo;
                } else if ($scope.eventActionRange == 'previous') {
                    dateVar = "previous"+ document.getElementById("eventActionPrevText").value;
                } else if ($scope.eventActionRange == 'last') {
                    dateVar = "last" + document.getElementById("eventActionLastText").value;
                } 
            } else {
                dateVar = $scope.eventActionDate;
            }

            myClient.api({
                method: methodVar,
                idSite: $scope.website.idsite,
                period: period,
                date: dateVar,
                segment: segmentVar
            }, function (err, responseObject) {
                loadEventsCombo(err, responseObject);
            });
        }

        function loadEventsCombo(err, responseObject) {
            var eventsList = [];
            if (!err) {
                console.log(responseObject);
                console.log("response: " + JSON.stringify(responseObject));
                var i = 0;
                for (var k in responseObject) {
                    eventsList[k] = responseObject[k].label;
                } 
                var someHtmlVar = '<select class="form-control" id="eventsCombo">';
                $scope.eventCategory = '';
                for(var k in eventsList) {
                    if(k == 0) {
                        $scope.eventCategory = eventsList[k];
                    }
                    someHtmlVar += '<option value="'+eventsList[k]+'">'+eventsList[k]+'</option>';
                }
                someHtmlVar += '</select>';
                $scope.customHtml = $sce.trustAsHtml(someHtmlVar);

                /* plot event actions depending on the event category */
                getEventCategoryFromScope = true;
                document.getElementById("changeCategoryBtn").disabled = false;
                plotEvents('eventActions');
            } else {
                console.log(err);
            } 
        }

        /* end event categories loading into combo box */

        function plotGraph(err, responseObject, type) {

            var labels = [];
            var visits = [];
            var events = [];
            var valueEvents = [];
            var sum = [];
            var avg = [];

            var enableTreeMapOne = false;

            //var eventsList = [];

            var scaleX = '';

            var treemapData;
            var treemapSeriesChildren = [];

            if(type == 'events') {
                scaleX = "Event Category";
            } else if (type == 'eventActions') {
                scaleX = "Event Actions";
            } else if (type == 'eventNames') {
                scaleX = "Event Names";
            }            


            if (!err) {
                //console.log(responseObject);
                //console.log("response: " + JSON.stringify(responseObject));
                var i = 0;

                if(type == 'events') {
                    for(var k in $scope.globalEvents) {
                        var found = false;
                        labels[k] = $scope.globalEvents[k];

                        for(var m in responseObject) {
                            if($scope.globalEvents[k] == responseObject[m].label) {
                                //console.log(responseObject[m].label);
                                visits[k] = responseObject[m].nb_visits;
                                events[k] = responseObject[m].nb_events;
                                valueEvents[k] = responseObject[m].nb_events_with_value;
                                sum[k] = responseObject[m].sum_event_value;
                                avg[k] = responseObject[m].avg_event_value;
                                
                                treemapSeriesChildren.push({"text":$scope.globalEvents[k],"value":sum[k]});

                                if(sum[k] != 0) {
                                    enableTreeMapOne = true;
                                }

                                found = true;
                                break;
                            }
                        }
                        if(!found) {
                            visits[k] = 0;
                            events[k] = 0;
                            valueEvents[k] = 0;
                            sum[k] = 0;
                            avg[k] = 0;
                            treemapSeriesChildren.push({"text":$scope.globalEvents[k],"value":0});
                        }
                    }   
                } else {
                    for (var k in responseObject) {
                        //if(type == 'events'){
                        //    eventsList[k] = responseObject[k].label;
                        //}
                        labels[k] = responseObject[k].label;
                        visits[k] = responseObject[k].nb_visits;
                        events[k] = responseObject[k].nb_events;
                        valueEvents[k] = responseObject[k].nb_events_with_value;
                        sum[k] = responseObject[k].sum_event_value;
                        avg[k] = responseObject[k].avg_event_value;
                    }
                }
                
                
                var chartData = {
                    "type": "bar", 
                    "legend": {},
                    "plotarea": {
                        "adjust-layout": true
                    },
                    "scale-x": {
                        "label": { 
                            "text": scaleX,
                        },
                        "labels": labels
                    },
                    "series": [ 
                        { "values": visits, "text": "Visits", "background-color": "#3c763d", "alpha": 0.9, "hover-state": { "background-color": "#3c763d" } },
                        { "values": events, "text": "Events", "background-color": "#f2dede", "alpha": 0.9, "hover-state": { "background-color": "#f2dede" } },
                        { "values": valueEvents, "text": "Events with value", "background-color": "#5bc0de", "alpha": 0.9, "hover-state": { "background-color": "#5bc0de" } },
                        { "values": sum, "text": "Sum event value", "background-color": "#d9534f", "alpha": 0.9, "hover-state": { "background-color": "#d9534f" } },
                        { "values": avg, "text": "Avg event value", "background-color": "#f0ad4e", "alpha": 0.9, "hover-state": { "background-color": "#f0ad4e" } }
                    ]
                };

                //console.log("Tree chart data: ");
                //console.log(JSON.stringify(treemapSeriesChildren));

                treemapData = {
                    "type":"treemap",
                    "series":[
                        {
                            "text":"Events map",
                            "children":treemapSeriesChildren
                        }
                    ]
                };

                treemapNullData = {
                    "type":"treemap",
                    "series":[
                        {
                            "text":"Events map",
                            "children":[]
                        }
                    ]
                };
                

                var divVar = '';
                var treeMapDivVar = '';

                if(type == 'events') {
                    divVar = 'chartDiv';
                    treeMapDivVar = 'treeChartDiv';
                } else if (type == 'eventActions') {
                    divVar = 'chartEventActionsDiv';
                    plotTreemap();
                } else if (type == 'eventNames') {
                    divVar = '';
                }

                zingchart.render({
                    id: divVar,
                    data: chartData
                });

                if(enableTreeMapOne) {
                    zingchart.render({
                        id: treeMapDivVar,
                        data: treemapData
                    });
                } else {
                    zingchart.render({
                        id: treeMapDivVar,
                        data: treemapNullData
                    });
                }
                
                
                $scope.$apply();
            } else {
                console.log(err);
            }                                                    
        }

        function plotTreemap() {
            var treemapData = {
                "type":"treemap",
                "series":[
                    {
                        "text":"Events map",
                        "children":[
                            {
                                "text":"Books",
                                "children":[
                                    {
                                        "text":"add-to-cart",
                                        "value":21
                                    },
                                    {
                                        "text":"view-details",
                                        "value":3
                                    }
                                ]
                            },
                            {
                                "text":"site-search",
                                "children":[
                                    {
                                        "text":"click-search",
                                        "value":12
                                    }
                                ]
                            }
                        ]
                    }
                ]
            };

            // zingchart.render({
            //     id: 'treemapEventActionsDiv',
            //     data: treemapData
            // });
        }

        

        function plotTimelineEvents() {
            
            var timeline = [];

            var amount = '';
            if ($scope.blockRange == 'previous') {
                amount = document.getElementById("prevBlockText").value;
            } else if ($scope.blockRange == 'last') {
                amount = document.getElementById("lastBlockText").value;
            }
            

            myClient.api({
                method: 'Events.getCategory',
                idSite: $scope.website.idsite,
                period: $scope.blockPeriod,
                date: $scope.blockRange+amount,
                segment: 'userId==' + $scope.userId + ''
            }, function (err, responseObject) {
                if (!err) {
                    console.log("timeline events: "+JSON.stringify(responseObject));

                    var events = $scope.globalEvents;   
                    
                    var i = 0;

                    for (var key1 in responseObject) {
                        var response = JSON.stringify(responseObject[key1]);
                        timeline[i++] = key1;
                        // for (var key2 in responseObject[key1]) {
                        //     var eventValue = ""+responseObject[key1][key2].label;
                        //     if (events.indexOf(eventValue) == -1) {
                        //         events.push(eventValue);
                        //     }
                        // }
                    }

                    // console.log("events: "+events);
                    
                    
                    var visits = [];
                    var eventsNos = [];
                    var valueEvents = [];
                    var sum = [];
                    var avg = [];

                    for(var k in events){
                        visits[k] = [];
                        eventsNos[k] = [];
                        valueEvents[k] = [];
                        sum[k] = [];
                        avg[k] = [];
                    }
                    
                    var j = 0;
                    for (var key1 in responseObject) {

                        var response = JSON.stringify(responseObject[key1]);
                        if (response == '[]') {
                            for (var k in events) {
                                visits[k][j] = 0;
                                eventsNos[k][j] = 0;
                                valueEvents[k][j] = 0;
                                sum[k][j] = 0;
                                avg[k][j] = 0;
                            }
                            j++;
                            continue;
                        }
                        var subEvents = [];
                        for (var key2 in responseObject[key1]) {
                            var eventValue = "" + responseObject[key1][key2].label;
                            subEvents.push(eventValue);
                            var found = false;
                            for (var k in events) {
                                if (events[k] == eventValue) {
                                    visits[k][j] = responseObject[key1][key2].nb_visits;
                                    eventsNos[k][j] = responseObject[key1][key2].nb_events;
                                    valueEvents[k][j] = responseObject[key1][key2].nb_events_with_value;
                                    sum[k][j] = responseObject[key1][key2].sum_event_value;
                                    avg[k][j] = responseObject[key1][key2].avg_event_value;

                                    //alert(eventValue +": "+visits[k][j]+" "+eventsNos[k][j]+" "+valueEvents[k][j]+" "+sum[k][j]+" "+avg[k][j]);
                                    found = true;
                                    break;
                                }
                            }
                        }

                        for (var k in events) {
                            if (subEvents.indexOf(events[k]) == -1) {
                                visits[k][j] = 0;
                                eventsNos[k][j] = 0;
                                valueEvents[k][j] = 0;
                                sum[k][j] = 0;
                                avg[k][j] = 0;
                            }
                        }

                        j++;
                    }


                    var i = 1;
                    for(var k in visits) {
                        console.log(i++);
                        console.log(visits[k][7]);
                    }


                    var i = 0;
                    var visitValues = [];
                    var eventsNosValues = [];
                    var valueEventsValues = [];
                    var sumValues = [];
                    var avgValues = [];
                    

                    for (var k in events) {
                        visitValues.push({ "values": visits[k], "text": events[k], "alpha": 0.9, });
                        eventsNosValues.push({ "values": eventsNos[k], "text": events[k], "alpha": 0.9, });
                        valueEventsValues.push({ "values": valueEvents[k], "text": events[k], "alpha": 0.9, });
                        sumValues.push({ "values": sum[k], "text": events[k], "alpha": 0.9, });
                        avgValues.push({ "values": avg[k], "text": events[k], "alpha": 0.9, });
                    }
                    
                    if ($scope.blockGraph == 'visits') {
                        var chartData = {
                            "type": "bar",
                            "plotarea": {
                                "adjust-layout": true
                            },
                            "legend": {},
                            "scale-x": {
                                "label": {
                                    "text": "Visits",
                                },
                                "labels": timeline
                            },
                            "series": visitValues
                        };
                        zingchart.render({
                            id: 'chartEventBlocksDiv',
                            data: chartData
                        });
                    } else if ($scope.blockGraph == 'events') {
                        var chartData = {
                            "type": "bar",
                            "legend": {},
                            "scale-x": {
                                "label": {
                                    "text": "Events",
                                },
                                "labels": timeline
                            },
                            "series": eventsNosValues
                        };
                        zingchart.render({
                            id: 'chartEventBlocksDiv',
                            data: chartData
                        });
                    } else if ($scope.blockGraph == 'eventValues') { 
                        var chartData = {
                            "type": "bar",
                            "legend": {},
                            "scale-x": {
                                "label": {
                                    "text": "Events with value",
                                },
                                "labels": timeline
                            },
                            "series": valueEventsValues
                        };
                        zingchart.render({
                            id: 'chartEventBlocksDiv',
                            data: chartData
                        });
                    } else if ($scope.blockGraph == 'sum') { 
                        var chartData = {
                            "type": "bar",
                            "legend": {},
                            "scale-x": {
                                "label": {
                                    "text": "Sum event value",
                                },
                                "labels": timeline
                            },
                            "series": sumValues
                        };
                        zingchart.render({
                            id: 'chartEventBlocksDiv',
                            data: chartData
                        });
                    } else if ($scope.blockGraph == 'avg') { 
                        var chartData = {
                            "type": "bar",
                            "legend": {},
                            "scale-x": {
                                "label": {
                                    "text": "Avg event value",
                                },
                                "labels": timeline
                            },
                            "series": avgValues
                        };
                        zingchart.render({
                            id: 'chartEventBlocksDiv',
                            data: chartData
                        });
                    }
                    
                    $scope.$apply();
                } else {
                    console.log(err);
                }
            });



        }
        
}]);