﻿var port = 1338;
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var fs = require('fs');

var PiwikClient = require('piwik-client');  // a core module
var myClient = new PiwikClient('http://adaptiveui.tk///piwik/index.php', 'bf8535dfb2511c611da95bd9a8e63e57');

var PythonShell = require('python-shell');

//app.use('/js', express.static(__dirname + '/client/js'));
app.use(express.static(__dirname + "/client"));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/client/views/index.html');
});

app.get('/Files/batch.csv', function (req, res) {
    res.sendFile(__dirname + '/client/python/batch.csv');
});

app.get('/Files/textrecord.json', function (req, res) {
    res.sendFile(__dirname + '/client/python/textrecord.json');
});

app.get('/Files/result.txt', function (req, res) {
    res.sendFile(__dirname + '/client/python/result.txt');
});

app.listen(port, function () {  // keep on listening on this port and handle http requests
    console.log('I\'m listening on port: '+port);
});

app.post('/dowork/:ID',function(req, res){

 	var id = req.params.ID;

  //console.log('came to dowork '+id);

	fs.writeFile('./client/python/result.txt','',function(err){
	  if(err)
	    console.error(err);
	  	//console.log('Written!');
	});

  //  var pyshell = new PythonShell('./client/python/AWS-boto3.py');
	 
	// pyshell.on('message', function (message) {
	// 	fs.appendFile('./client/python/result.txt',message,function(err){
	// 	  if(err)
	// 	    console.error(err);
	// 	  console.log('Appended!');
	// 	});
	// });
	 
	// pyshell.end(function (err) {
	//   if (err) throw err;
	//   console.log('finished');
	// });

  PythonShell.run('./client/python/AWS-boto3.py', function (err) {
    if (err) {
      console.log(err);
    }
    //console.log('finished');
  });

  res.write('success');
  res.end();

});

app.post('/writeData',function(req, res){
    //console.log(req.body);
    var data = JSON.stringify(req.body);
    //var data = '{"Id":"1","Elevation":"2596.0","Aspect":"51.0","Slope":"3.0","Horizontal_Distance_To_Hydrology":"258.0","Vertical_Distance_To_Hydrology":"0.0","Horizontal_Distance_To_Roadways":"510.0","Hillshade_9am":"221.0","Hillshade_Noon":"232.0","Hillshade_3pm":"148.0","Horizontal_Distance_To_Fire_Points":"6279.0","Wilderness_Area1":"1.0","Wilderness_Area2":"0.0","Wilderness_Area3":"0.0","Wilderness_Area4":"0.0","Soil_Type1":"0.0","Soil_Type2":"0.0","Soil_Type3":"0.0","Soil_Type4":"0.0","Soil_Type5":"0.0","Soil_Type6":"0.0","Soil_Type7":"0.0","Soil_Type8":"0","Soil_Type9":"0.0","Soil_Type10":"0.0","Soil_Type11":"0.0","Soil_Type12":"0","Soil_Type13":"0.0","Soil_Type14":"0.0","Soil_Type15":"0.0","Soil_Type16":"0","Soil_Type17":"0.0","Soil_Type18":"0","Soil_Type19":"0","Soil_Type20":"0","Soil_Type21":"0.0","Soil_Type22":"0.0","Soil_Type23":"0","Soil_Type24":"0","Soil_Type25":"0.0","Soil_Type26":"0.0","Soil_Type27":"0.0","Soil_Type28":"0.0","Soil_Type29":"1","Soil_Type30":"0","Soil_Type31":"0.0","Soil_Type32":"0.0","Soil_Type33":"0.0","Soil_Type34":"0.0","Soil_Type35":"0.0","Soil_Type36":"0.0","Soil_Type37":"0.0","Soil_Type38":"0.0","Soil_Type39":"0.0","Soil_Type40":"0"}';
    fs.writeFile('./client/python/testrecord.json',data,function(err){
      if(err)
        console.error(err);
      //console.log('Written to textrecord.json!');
    });
});

app.post('/writeBatchData',function(req, res){
    //console.log(req.body);
    //var data = JSON.stringify(req.body);
    var data = req.body.batch;

    fs.writeFile('./client/python/batch.csv',data,function(err){
      if(err)
        console.error(err);
      //console.log('Written to textrecord.json!');
      res.write('success');
      res.end();
    });
});



app.get('/getProfile/:WEBSITEID/:USERID',function(req, res){

    //var websiteid = req.body.websiteid;
    //var userid = req.body.userid;

    var websiteid = req.params.WEBSITEID;
    var userid = req.params.USERID;

    ////////////////////////////////

    var createdDate = '2015-12-01';
    var globalEvents = ["search","bookflights","popular","reserve","discount"];

    ////////////////////////////////

            myClient.api({
                method: 'Events.getCategory',
                idSite: websiteid,
                period: 'range',
                date: ''+createdDate+',today',
                segment: 'userId==' + userid
            }, function (err, responseObject) {
                if (!err) {
                    console.log(JSON.stringify(responseObject));

                    var jsonData = '{"Id":"'+userid+'",';

                    var len = globalEvents.length - 1;

                    // write json data to file to be sent to AWS
                    for(var k in globalEvents) {
                        var found = false;
                        var e = globalEvents[k];
                        for(var m in responseObject) {
                            if(globalEvents[k] == responseObject[m].label) {
                                jsonData += '"'+e+'_visits":"'+responseObject[m].nb_visits+'","'+e+'_events":"'+responseObject[m].nb_events+'","'+e+'_events_with_value":"'+responseObject[m].nb_events_with_value+'","'+e+'_sum_event_value":"'+responseObject[m].sum_event_value+'","'+e+'_min_event_value":"'+responseObject[m].min_event_value+'","'+e+'_max_event_value":"'+responseObject[m].max_event_value+'","'+e+'_sum_daily_nb_uniq_visitors":"'+responseObject[m].sum_daily_nb_uniq_visitors+'","'+e+'_avg_event_value":"'+responseObject[m].avg_event_value+'"';
                                if(k!=len) {
                                    jsonData += ',';    
                                }
                                found = true;
                                break;
                            }
                        }
                        if(!found) {
                            jsonData += '"'+e+'_visits":"0","'+e+'_events":"0","'+e+'_events_with_value":"0","'+e+'_sum_event_value":"0","'+e+'_min_event_value":"0","'+e+'_max_event_value":"0","'+e+'_sum_daily_nb_uniq_visitors":"0","'+e+'_avg_event_value":"0"';
                            if(k!=len) {
                                jsonData += ',';  
                            }
                        }
                    }

                    jsonData += '}';

                    /////////////// start write data ////////////////

                    var data = JSON.stringify(req.body);

                    fs.writeFile('./client/endpoint/testrecord.json',jsonData,function(err){

                      if(err) {
                        console.error(err);
                      } else {
                        console.log('Written to /endpoint/textrecord.json!');
                      
                        //////////////// execute python scipt ////////////////////

                        var pyshell = new PythonShell('./client/endpoint/AWS-boto3.py');
     
                        pyshell.on('message', function (message) {
                          console.log(message);
                          res.write(message);
                          res.end();
                        });
                         
                        pyshell.end(function (err) {
                          if (err){
                            console.log(err);
                          } else {
                            console.log('finished simulation of endpoint');
                          }
                        });
                      }


                    });

                } else {
                    console.log(err);
                }
            });

});